import React, { Fragment } from "react";
// Component
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
// Pages
import Payment from "./pages/Payment/Payment";

function App() {
  return (
    <Fragment>
      {/* Header */}
      <div
        className="container-fluid fixed"
        style={{ boxShadow: "0px 2px 10px 0px rgba(14, 80, 164, 0.3)" }}
      >
        <Header />
      </div>
      {/* Page */}
      <div className="page-container">
        <Payment />
      </div>
      {/* Footer */}
      <div className="container-fluid">
        <Footer />
      </div>
    </Fragment>
  );
}

export default App;
