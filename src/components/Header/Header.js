import React from "react";
// CSS
import "./header.css";
// Img
import Logo from "../../assets/logo.jpg";
// Icon
import Search from "../../assets/search.svg";
import Bag from "../../assets/bag.svg";
import Cart from "../../assets/cart.svg";
import Heart from "../../assets/heart.svg";
import Notify from "../../assets/notify.svg";
import User from "../../assets/user.svg";

const Header = () => {
  return (
    <div className="container header-container">
      {/* Link list */}
      <ul className="links-list">
        <li className="links-item">
          <a href="#" className="h5">
            {" "}
            Tuyển dụng
          </a>
        </li>
        <li className="links-item">
          <a href="#" className="h5">
            {" "}
            Tải ứng dụng
          </a>
        </li>
        <li className="links-item">
          <a href="#" className="h5">
            {" "}
            Kết nối
          </a>
        </li>
        <li className="links-item">
          <a href="#" className="h5">
            {" "}
            Tin tức
          </a>
        </li>
      </ul>
      {/* Search */}
      <div className="header-content-wrapper">
        <div className="header-logo logo logo-big">
          <img src={Logo} alt="" />
        </div>
        <div className="header-search">
          <div className="header-search-input">
            <input type="text" placeholder="Tìm kiếm sản phẩm" />
            <div className="header-search-button">
              <img src={Search} alt="searc-button" />
            </div>
          </div>
          <ul className="header-search-links">
            <li>
              <a className="h6" href="#">
                Khác
              </a>
            </li>
            <li>
              <a className="h6" href="#">
                Chăm sóc cuộc sống
              </a>
            </li>
            <li>
              <a className="h6" href="#">
                Đồ dùng gia đình
              </a>
            </li>
            <li>
              <a className="h6" href="#">
                Mẹ và bé
              </a>
            </li>
            <li>
              <a className="h6" href="#">
                Thời trang nam
              </a>
            </li>
          </ul>
        </div>
        {/* Icon Group */}
        <ul className="header-icons">
          <li className="header-icon">
            <div>
              <img src={Bag} alt="header-icon" />
            </div>
            <a className="h6 active" href="#">
              Đơn hàng
            </a>
          </li>
          <li className="header-icon">
            <div>
              <img src={Heart} alt="header-icon" />
            </div>
            <a className="h6" href="#">
              Yêu thích
            </a>
          </li>
          <li className="header-icon">
            <div>
              <img src={Cart} alt="header-icon" />
            </div>
            <a className="h6" href="#">
              Giỏ hàng
            </a>
          </li>
          <li className="header-icon">
            <div>
              <img src={Notify} alt="header-icon" />
            </div>
            <a className="h6" href="#">
              Thông báo
            </a>
          </li>
          <li className="header-icon">
            <div>
              <img src={User} alt="header-icon" />
            </div>
            <a className="h6" href="#">
              Tài khoản
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Header;
