import React from "react";
// CSS
import "./footer.css";
// Logo
import LogoTrans from "../../assets/logo-trans.png";
import LogoCentif from "../../assets/certificate-logo.png";

// Icon
import Facebook from "../../assets/facebook.svg";
import Youtube from "../../assets/youtube.svg";
import Visa from "../../assets/visa.svg";
import MasterCard from "../../assets/master-card.svg";
import JCB from "../../assets/JCB.svg";
import Paypal from "../../assets/paypal.svg";
import GooglePlay from "../../assets/google-play.png";
import AppStore from "../../assets/app-store.png";
import Message from "../../assets/message-icon.png";

const Footer = () => {
  return (
    <div className="footer-container">
      <div className="container">
        {/* Logo */}
        <div className="logo logo-big">
          <img src={LogoTrans} alt="logo" />
        </div>
        {/* About */}
        <div className="footer-about">
          <div className="footer-about-left">
            <ul>
              <li>
                <a className="h4 bold" href="#">
                  CÔNG TY CỔ PHẦN ĐẦU TƯ VÀ SẢN XUẤT HOÀNG SA
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Mã số thuế: 0107747933
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Địa chỉ: Xóm Hàng Gậu, Đội 7, xã Chàng Sơn, huyện Thạch Thất,
                  thành phố Hà Nội, Việt Nam
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Điện thoại: 0938895622
                </a>
              </li>
            </ul>
          </div>
          <div className="footer-about-right">
            <ul>
              <li>
                <a className="h4" href="#">
                  Về chúng tôi
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Trang chủ
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Về chúng tôi
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Tuyển dụng
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <a className="h4 bold" href="#">
                  Dịch vụ khách hàng
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Chăm sóc khách hàng
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Liên hệ chúng tôi
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Chính sách và quy tắc
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <a className="h4 bold" href="#">
                  Dịch vụ thương mại
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Đảm bảo thương mại
                </a>
              </li>
              <li>
                <a className="h5" href="#">
                  Danh tính doanh nghiệp
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* Certification */}
        <div className="footer-certificate">
          {/* Left */}
          <div className="footer-certificate-left">
            <div className="logo logo-centificate">
              <img src={LogoCentif} alt="" />
            </div>
            <div className="footer-certificate-info">
              <ul className="footer-certificate-list">
                <li className="h5 footer-certificate-item">
                  Địa chỉ văn phòng đặt tại trụ sở: Số 41 đường số 12, KDC
                  ParkHills CityLand, phường 10, Gò Vấp, Hồ Chí Minh <br />
                  HiHiHi nhận đặt hàng trực tuyến và giao tận nơi, chưa hỗ trợ
                  mua và nhận hàng trực tiếp tại văn phòng hoặc trung tâm xử lý
                  đơn hàng
                </li>
                <li className="h5 footer-certificate-item">
                  Giấy chứng nhận Đăng ký Kinh doanh số 0107747933 do Sở Kế
                  Hoạch và Đầu Tư Thành Phố Hà Nội cấp ngày 07 tháng 03 năm 2017
                </li>
                <li className="h5 footer-certificate-item">
                  2021 - Bản quyền của Công Ty Cổ Phần Đầu Tư và Sản Xuất Hoàng
                  Sa
                </li>
              </ul>
            </div>
          </div>
          {/* Right */}
          <div className="footer-certificate-right">
            {/* Wrapper */}
            <div className="footer-certificate-right-wrapper">
              {/* Social Media */}
              <div className="footer-social-media">
                <div className="footer-certificate-header h4">Mạng xã hội</div>
                <div className="footer-social-media-icons">
                  <div className="footer-social-media-icon">
                    <img src={Facebook} alt="social-media-icon" />
                  </div>
                  <div className="footer-social-media-icon">
                    <img src={Youtube} alt="social-media-icon" />
                  </div>
                </div>
              </div>
              {/* Payment */}
              <div className="footer-payment">
                <div className="footer-certificate-header h4">Thanh Toán</div>
                <div className="footer-payment-icons">
                  <div className="footer-payment-icon">
                    <img src={Visa} alt="payment-icon" />
                  </div>
                  <div className="footer-payment-icon">
                    <img src={MasterCard} alt="payment-icon" />
                  </div>
                  <div className="footer-payment-icon">
                    <img src={JCB} alt="payment-icon" />
                  </div>
                  <div className="footer-payment-icon">
                    <img src={Paypal} alt="payment-icon" />
                  </div>
                </div>
              </div>
            </div>
            {/* Store */}
            <div className="footer-store">
              <div className="footer-certificate-header h4">Tải Ứng Dụng</div>
              <div className="footer-store-icons">
                <div className="footer-store-icon">
                  <img src={GooglePlay} alt="store-icon" />
                </div>
                <div className="footer-store-icon">
                  <img src={AppStore} alt="store-icon" />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Message */}
        <div className="footer-message">
          <div className="footer-message-lable h6">
          Liên hệ nhà cung cấp !
          </div>
          <div className="footer-message-icon">
            <img src={Message} alt="message-icon" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
