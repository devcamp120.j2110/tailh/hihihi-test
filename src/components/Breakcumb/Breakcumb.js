import React from "react";
// Icon
import breakcumb from "../../assets/breakcumb.svg";
// CSS
import "./breakcumb.css";

const Breakcumb = () => {
  return (
    <div className="breakcumb-container">
      <a className="h4" href="#">
        Trang chủ
      </a>
      <div className="breakcumb-icon">
        <img src={breakcumb} alt="breakcumb-icon" />
      </div>
      <a className="h4" href="#">
        Giỏ hàng
      </a>
      <div className="breakcumb-icon">
        <img src={breakcumb} alt="breakcumb-icon" />
      </div>
      <a className="h4 active" href="#">
        Thanh toán
      </a>
    </div>
  );
};

export default Breakcumb;
