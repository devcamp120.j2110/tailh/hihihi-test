import React, { Fragment } from "react";
// Components
import Breakcumb from "../../components/Breakcumb/Breakcumb";
// CSS
import "./payment.css";
// Icon
import StoreIcon from "../../assets/store-icon.svg";
import LocationIcon from "../../assets/location-icon.svg";
import VoucherIcon from "../../assets/voucher-icon.svg";
import CarIcon from "../../assets/car-icon.svg";
// Img
import Product1 from "../../assets/product1.png";
import Product2 from "../../assets/product2.png";
import Product3 from "../../assets/product3.png";
// Logo
import Logo from "../../assets/logo.jpg";

const Payment = () => {
  return (
    <Fragment>
      {/* Breakcumb */}
      <Breakcumb />
      {/* Cart */}
      <div className="cart-wrapper">
        {/* Cart */}
        <div className="cart">
          {/* Header */}
          <div className="cart-header">
            <div className="cart-header-name h3">Sản Phẩm</div>
            <div className="cart-header-group">
              <div className="price h5">Đơn giá</div>
              <div className="quantity h5">Số lượng</div>
              <div className="total h5">Thành tiền</div>
            </div>
          </div>
          {/* End Header */}

          {/* Store */}
          <div className="cart-store">
            <div className="cart-store-icon">
              <img src={StoreIcon} alt="store-icon" />
            </div>
            <div className="cart-store-name">
              <a href="#" className="h4">
                Nongtrai Official Store
              </a>
            </div>
          </div>
          {/* End Store */}

          {/* Product */}
          <div className="cart-product-wrapper">
            <div className="cart-product">
              {/* Product Detail*/}
              <div className="cart-product-detail">
                {/* Product Img*/}
                <div className="product-detail-img">
                  <img src={Product1} alt="product-img" />
                </div>
                {/* Product Content*/}
                <div className="product-content">
                  <h4 className="h4 product-content-name">
                    Rau củ tươi sản xuất tại nông trại VN
                  </h4>
                  <h6 className="product-content-description h6">Lẻ:100g</h6>
                </div>
              </div>
              {/* Product Price*/}
              <div className="cart-product-price">
                <h5 className="h5">25.000VND</h5>
                <h5 className="h5">1</h5>
                <h5 className="h5">25.000VND</h5>
              </div>
            </div>
            <div className="cart-product">
              {/* Product Detail*/}
              <div className="cart-product-detail">
                {/* Product Img*/}
                <div className="product-detail-img">
                  <img src={Product2} alt="product-img" />
                </div>
                {/* Product Content*/}
                <div className="product-content">
                  <h4 className="h4 product-content-name">
                    Rau củ tươi sản xuất tại nông trại VN
                  </h4>
                  <h6 className="product-content-description h6">Lẻ:100g</h6>
                </div>
              </div>
              {/* Product Price*/}
              <div className="cart-product-price">
                <h5 className="h5">25.000VND</h5>
                <h5 className="h5">1</h5>
                <h5 className="h5">25.000VND</h5>
              </div>
            </div>
            <div className="cart-product">
              {/* Product Detail*/}
              <div className="cart-product-detail">
                {/* Product Img*/}
                <div className="product-detail-img">
                  <img src={Product3} alt="product-img" />
                </div>
                {/* Product Content*/}
                <div className="product-content">
                  <h4 className="h4 product-content-name">
                    Rau củ tươi sản xuất tại nông trại VN
                  </h4>
                  <h6 className="product-content-description h6">Lẻ:100g</h6>
                </div>
              </div>
              {/* Product Price*/}
              <div className="cart-product-price">
                <h5 className="h5">25.000VND</h5>
                <h5 className="h5">1</h5>
                <h5 className="h5">25.000VND</h5>
              </div>
            </div>
          </div>
          {/* End Product */}

          {/* Footer */}
          <div className="cart-footer">
            {/* Message */}
            <div className="cart-footer-message">
              <lable className="h5" htmlFor="cart-message">
                Ghi chú:{" "}
              </lable>
              <input
                type="text"
                placeholder="Để lại ghi chú cho người bán..."
              />
            </div>
            {/* Total */}
            <div className="cart-footer-total">
              <h4 className="h4">Tổng tiền ( 3 sản phẩm):</h4>
              <h3 className="h3">75.000 VND</h3>
            </div>
          </div>
          {/* End Footer */}
        </div>
        {/* End Cart */}

        {/* Payment */}
        <div className="payment">
          {/* Payment Address */}
          <div className="payment-address">
            {/* Header */}
            <div className="payment-address-header">
              <div className="payment-address-icon">
                <img src={LocationIcon} alt="address-icon" />
              </div>
              <h4 className="h4 payment-address-title">Địa chỉ nhận hàng</h4>
            </div>
            {/* Content */}
            <h5 className="h5 payment-address-content">
              <a href="#">+ Thêm địa chỉ mới</a>
            </h5>
          </div>
          {/*End Payment Address */}

          {/* Payment Voucher */}
          <div className="payment-voucher">
            <div className="payment-voucher-header">
              <div className="payment-voucher-icon">
                <img src={VoucherIcon} alt="voucher-icon" />
              </div>
              <h4 className="h4 payment-voucher-title">Voucher</h4>
            </div>
            <button className="h4">Chọn</button>
          </div>
          {/*End Payment Voucher */}

          {/* Payment Transport */}
          <div className="payment-transport">
            {/* Header */}
            <div className="payment-transport-header">
              <div className="payment-transport-icon">
                <img src={CarIcon} alt="transport-icon" />
              </div>
              <h4 className="h4 payment-transport-title">Voucher</h4>
            </div>
            {/* Button */}
            <button className="h4">Chọn</button>
          </div>
          {/*End Payment Transport */}

          {/* Payment Info */}
          <div className="payment-info">
            <input type="checkbox" id="payment-info-input" value="" />
            <label for="payment-info-input" className="h4">
              Thông tin xuất hoá đơn
            </label>
          </div>
          {/*End Payment Info */}

          {/* Payment Methods */}
          <div className="payment-methods">
            {/* Header */}
            <h3 className="payment-methods-header">Phương thức thanh toán</h3>

            {/* Input Group */}
            <div className="payment-methods-radios">
              {/* Radio Top */}
              <div className="payment-methods-radios-top">
                <div className="radio-wrapper">
                  <input
                    type="radio"
                    id="hihihi-wallet"
                    name="wallet"
                    value=""
                  />
                  <label className="h6" for="hihihi-wallet">
                    Ví HiHiHi
                  </label>
                </div>
                <div className="radio-wrapper">
                  <input
                    type="radio"
                    id="paypal-wallet"
                    name="wallet"
                    value=""
                  />
                  <label className="h6" for="paypal-wallet">
                    Paypal
                  </label>
                </div>
              </div>
              {/* Radio Bottom */}
              <div className="payment-methods-radios-bottom">
                <div className="radio-wrapper">
                  <input type="radio" id="bank-wallet" name="wallet" value="" />
                  <label className="h6" for="bank-wallet">
                    Chuyển khoản ngân hàng
                  </label>
                </div>
              </div>
            </div>

            {/* Wallet */}
            <div className="payment-methods-wallet">
              <div className="payment-methods-wallet-header">
                <div className="logo logo-small">
                  <img src={Logo} alt="logo" />
                </div>
                <h4 className="h4">+ Nạp thêm</h4>
              </div>
              <div className="payment-methods-wallet-surplus">
                <h5 className="h5">Số Dư Ví HIHIHI:</h5>
                <h3 className="h3">150.000 Xu</h3>
              </div>
            </div>

            {/* Order Detail */}
            <div className="payment-methods-order-wrapper">
              {/* Order */}
              <div className="payment-methods-order">
                {/* Order Item*/}
                <div className="payment-methods-order-item">
                  <h4 className="h4">Tổng tiền (tạm tính):</h4>
                  <h4 className="h4">75.000VND</h4>
                </div>
                {/* Order Item*/}
                <div className="payment-methods-order-item">
                  <h4 className="h4">Phí vận chuyển:</h4>
                  <h4 className="h4">20.000VND</h4>
                </div>
                {/* Order Item*/}
                <div className="payment-methods-order-item">
                  <h4 className="h4">Phí giao dịch</h4>
                  <h4 className="h4">0 VND</h4>
                </div>
                {/* Order Item*/}
                <div className="payment-methods-order-item">
                  <h4 style={{ color: "rgba(9, 82, 163, 1)" }} className="h4">
                    Giảm giá
                  </h4>
                  <h4 className="h4">5.500 VND</h4>
                </div>
              </div>

              {/* Payment Total */}
              <div className="payment-methods-total">
                <div className="payment-methods-order-item">
                  <h4 className="h4">Tổng tiền hàng:</h4>
                  <h3 className="h3">105.500 VND</h3>
                </div>
              </div>

              {/* Payment Order Button */}
              <div className="payment-methods-button h5">Đặt Hàng</div>

              {/* Payment Rules */}
              <div className="payment-methods-rules">
                Nhấn “Đặt hàng” đồng nghĩa với việc bạn đồng ý<br />
                <a className="h6" href="#">
                  Điều khoản của HIHIHI
                </a>
              </div>
            </div>
          </div>
          {/*End Payment Methods */}
        </div>
        {/* End Payment */}
      </div>
    </Fragment>
  );
};

export default Payment;
